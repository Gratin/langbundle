# Using the Language Switcher Bundle

- [ ] Requirements
- [ ] Require the bundle
- [ ] Configuring the bundle and it's dependencies
- [ ] Custom configurations
- [ ] Usage

## Requirements

As of now, since this bundle depends on two other bundle, the symfony version needs to be 2.8 or lower.

- [x] Requirements
- [ ] Require the bundle
- [ ] Configuring the bundle and it's dependencies
- [ ] Custom configurations
- [ ] Usage

## Require the bundle

Until the package is on packagist or any other package distribution platform, you need to add this to your project's composer.json:

```php
   [...]
   "require" : {
        [...]
        "kryzalid/lang-switcher" : "dev-master"
     },
     "repositories" : [{
        "url" : "https://Gratin@bitbucket.org/Gratin/langbundle.git",
        "type" : "git"
     }],
```

Run composer install. The first step is completed.

- [x] Requirements
- [x] Require the bundle
- [ ] Configuring the bundle and it's dependencies
- [ ] Custom configurations
- [ ] Usage

## Configuring the bundle and it's dependencies

Since the bundle requires 2 other symfony bundles ([BeSimplei18n Routing](https://github.com/BeSimple/BeSimpleI18nRoutingBundle) and [KnpLabs Doctrine Behaviors](https://github.com/KnpLabs/DoctrineBehaviors)) we need to configure them. Go ahead and look through those bundle to see a complete list of configuration, the minimum required to use this bundle is as simple as this:

```yml
be_simple_i18n_routing: ~
knp_doctrine_behaviors:
    translatable:   true
```

Now you can add theses three lines inside your AppKernel.php

```php
    new BeSimple\I18nRoutingBundle\BeSimpleI18nRoutingBundle(),
    new Knp\DoctrineBehaviors\Bundle\DoctrineBehaviorsBundle(),
    new Kryzalid\LangBundle\KryzalidLangBundle(),
```

One last thing we need to specify the locales used inside our parameters.yml file.

```yml
 locales: [fr, en, es]
```

Add as many local aliases as necessary.

We're almost done! As of now, the bundle will work as intended but only with one url parameter (slug). If you don't need anything else, then go to the "Usage" section.

- [x] Requirements
- [x] Require the bundle
- [x] Configuring the bundle and it's dependencies
- [ ] Custom configurations
- [ ] Usage

## Custom configuration

Other options are available!

### The translation_mapping parameter

```yml
  translation_mapping:
     key: Repository function name
```

With this parameter, you have the power to decide which fuction is called for which key.
For example:

```yml
  translation_mapping:
     slug: findOneBySlugAndLocale
```

It is **important** to note that those repository function are called with 2 arguments, the first is always the value to translate and the second is the locale for which to get the translation.

The default function for a slug is findSlugged (which you can find under Kryzalid\LangBundle\Repository\TranslatableRepository)

```php
   public function findSlugged($slug, $locale = 'fr')
    {
        $qb = $this->createQueryBuilder('t');

        $qb
            ->select('t', 'tt')
            ->join('t.translations', 'tt')
            ->where('tt.slug = :slug')
            ->andWhere("tt.locale = :locale")
            ->setParameter('slug', $slug)
            ->setParameter('locale', $locale);

        return $qb->getQuery()->getOneOrNullResult();
    }
```

### The exclude_from_translation parameter

```yml
 exclude_from_translation: [...routes_prefix]
```

This parameters lets you exclude routes from the exhaustive translation process.
Let's say you have a static url like the News page homepage:

In your routing.yml:

```yml
home_news:
    pattern:     /nouvelles
    locales: {fr: "/nouvelles", en: "/news", es: "/noticias" }
    defaults: { _controller: KryzalidLangBundle:Default:news }
```

You could add this in your parameter.yml:

> exclude_from_translation: [home]

This way, when the code finds out that your route is prefixed with home, it will skip the process and simply return the urls using the simple "route_name.locale" method.

- [x] Requirements
- [x] Require the bundle
- [x] Configuring the bundle and it's dependencies
- [X] Custom configurations
- [ ] Usage

## Usage

And finally! There are quite a few things to normalize in your codebase. To facilitate the correspondance between routes and entities, you need to prefix your routes with the entity name.

Let's say you have a New entity and you want to translate the view of a single news:

```yml
 news_details:
    pattern:     /nouvelles/{slug}
    locales: {fr: "/nouvelles/{slug}", en: "/news/{slug}", es: "/noticias/{slug}" }
    defaults: { _controller: KryzalidLangBundle:Default:news }
```

This will tell the program to use "News" as the entity (and therefore it's repository). You can use as many underscore as you wish (car_new would return a CarNew entity, etc.), you get the gist.

And the final thing, if you want to make the most basic usage of this bundle, without any configuration, you **will** need to have your repositories extend the TranslatableRepository bundled with the package.

```php
 use Kryzalid\LangBundle\Repository\TranslatableRepository;
```

This will add the "findSlugged" function to your repository. If you want to have your own custom functions, there is only one requirements: the first arguments in your function needs to be the value of the translatable key and the second needs to be the locale to be queried.

Finally, use this twig function call inside of a template and you're good to go!
If you have only 2 locales:

```twig
 {{ switchLang() }}
```

If you have more:

```twig
 {% set langs = switchLang() %}
 {% for lang in langs %}
 <a href="{{ lang['uri'] }}">{{ lang['name'] }}</a>
```

- [x] Requirements
- [x] Require the bundle
- [x] Configuring the bundle and it's dependencies
- [X] Custom configurations
- [X] Usage

# We are done!

I hope you enjoy this small simple but effective bundle.