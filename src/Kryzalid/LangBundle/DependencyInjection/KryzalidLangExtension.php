<?php

namespace Kryzalid\LangBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class KryzalidLangExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        if (isset($processedConfig['knp_doctrine_behaviors'])) {
            $container->setParameter('knp_doctrine_behaviors', $processedConfig['knp_doctrine_behaviors']);
        } else {
            $container->setParameter('knp_doctrine_behaviors', false);
        }

        if (isset($processedConfig['be_simple_i18n_routing'])) {
            $container->setParameter('be_simple_i18n_routing', $processedConfig['be_simple_i18n_routing']);
        } else {
            $container->setParameter('be_simple_i18n_routing', false);
        }
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
