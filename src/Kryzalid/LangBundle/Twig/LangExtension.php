<?php
namespace Kryzalid\LangBundle\Twig;

class LangExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $urlGen;
    private $em;

    private $defaultsMapping = [
        'slug'  =>  "findSlugged",
    ];

    public function __construct($container, $urlGen, $entityManager)
    {
        $this->container = $container;
        $this->urlGen = $urlGen;
        $this->em = $entityManager;
    }

    public function getFunctions()
    {
        return array(
           'switchLang' => new \Twig_Function_Method($this, 'switchLang')
        );
    }

    public function switchLang($route = null, $parameters = array())
    {
        /* Tests if knplabs/doctrine-behavior bundle is enabled and BeSimplei18nRouting. The only supported bundle yet. */
        if (!$this->container->hasParameter('knp_doctrine_behaviors')
            || !$this->container->hasParameter('be_simple_i18n_routing')
            ) {
            throw new \Exception("Le plugin ne fonctionne qu'avec le bundle KnpLabs\doctrine-behaviors 1.1 et plus pour l'instant.");
        }
        /* Route name prefix to exclude from translation (ex api, etc.)*/
        $excludes = array();
        if ($this->container->hasParameter('exclude_from_translation')) {
            $excludes = $this->container->getParameter('exclude_from_translation');
        }
        /* List of website locales */
        $locales = $this->container->getParameter('locales');

        /* Route and Request information */
        $request = $this->container->get('request');
        $route = $request->get('_route');
        $params = $request->get('_route_params');

        /* Current Locale */
        $oldLocale = $request->getLocale();
        /* Do we have more than one extra locale? Used to determine wether we should return an array of URIS or a single URI */
        $multiple = false;
        $localesToGet = array();
        
        foreach ($locales as $otherLocale) {
            if ($oldLocale != $otherLocale) {
                $localesToGet[] = $otherLocale;
            }
        }

        if (count($localesToGet) > 1) {
            $multiple = true;
        }

        /* Lets make the route in an array */
        $realRoute = explode('.', $route);
        $realRoute = explode('_', $realRoute[0]);

        $copy = $params;
        /* Removes the _locale parameter */
        array_shift($copy);
        /* Is it the homepage? Or are there no parameters to the route? Or is the route prefix in the exclusion list */
        if ($route == "homepage" || empty($copy) || in_array($realRoute[0], $excludes)) {
            if ($multiple) {
                $routes = [];
                foreach ($localesToGet as $loc) {
                    $routes[] = array(
                        'uri' => $this->urlGen->generate($route.".".$loc, $params, true),
                        'name'  =>  $loc,
                        );
                }
                return $routes;
            } else {
                $locale = array_map(function ($cell) {
                    if ($oldLocale != $cell) {
                        return $cell;
                    }
                }, $locales);
                $route = $this->urlGen->generate($route.".".$locale, $params, true);
                return $route;
            }
        }
        
        /* Let's list all our entities */
        $entities = array();
        $meta = $this->em->getMetadataFactory()->getAllMetadata();
        foreach ($meta as $m) {
            $name = explode('\\', $m->getName());
            $entities[end($name)] = array('class'=>$m->getName(), 'table'=>$m->getTableName());
        }

        /* In case your url ends with details */
        if (strpos(end($realRoute), 'details') !== false) {
            array_pop($realRoute);
            reset($realRoute);
        }
        
        /*
            Take the route name and uppercase it so we have a pretty string to use as a class name
         */
        $className = array_map(function ($cell) {
            return ucfirst($cell);
        }, $realRoute);

        /* Let's merge every part into a single string */
        $className = implode('', $className);
    
        /*
        Does the class exists     
         */
        if (!isset($entities[$className])) {
            throw new \Exception('The route name does not match any class. ["$className"]');
        }

        /*
        Find entity by className then uses the namspace with the className as a reflection
        Then gets the repository for the current class
         */
        $entityData = $entities[$className];
        $class = $entityData['class'];
        $reflection = new $class;
        $repo = $this->em->getRepository($class);

        if ($this->container->hasParameter('translation_mapping')) {
            $mappings = $this->container->getParameter('translation_mapping');
        } else {
            $mappings = $this->defaultsMapping;
        }

        $routes = [];
        $found = false;
        foreach ($mappings as $key => $function) {
            if (array_key_exists($key, $params)) {
                if (!method_exists($repo, $mappings[$key])) {
                    throw new \Exception("The repo function for key ($key) does not exists in the repository class.");
                }
                $entity = call_user_func(array($repo, $mappings[$key]), $params[$key], $oldLocale);
                if (!$entity) {
                    throw new \Exception("No entity was found within the repository's {$mappings[$key]} function with parameters $key: ".$params[$key] . " and with locale: ". $oldLocale);
                }

                $tableName = $entityData['table'].'_translation';

                /* Loops through all the necessary locales and fetches the slug for the right class */
                foreach ($localesToGet as $queriedLocale) {
                    $query = 'SELECT * FROM '.$tableName.' WHERE translatable_id = '.$entity->getId().' AND locale = "'.$queriedLocale.'" LIMIT 1';
                    $stmt = $this->em->getConnection()->prepare($query);
                    $stmt->execute();
                    $results = $stmt->fetchAll();
              
                    $params['_locale'] = $queriedLocale;
                    $params[$key] = $results[0][$key];
                    $routes[] = array(
                            'uri' => $this->urlGen->generate($route.".".$queriedLocale, $params, true),
                            'name'  =>  $queriedLocale,
                    );
                    //$this->urlGen->generate($route.".".$queriedLocale, $params, true);

                }
            }
            
            if (count($routes) > 1) {
                return $routes;
            } else {
                return $routes[0];
            }
        }
    }

    public function getName()
    {
        return 'langext';
    }
}
