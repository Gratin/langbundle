<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    public function testasAction($slug)
    {
        $repo = $this->getDoctrine()->getRepository("KryzalidLangBundle:Testing");
        $locale = $request->getLocale();
        $t = $repo->findSlugged($slug, $locale);
        return $this->render('default/test.html.twig', [
            't' =>  $t,
        ]);
    }
}
